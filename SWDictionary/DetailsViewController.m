//
//  DetailsViewController.m
//  SWDictionary
//
//  Created by Dimitar Dimitrov on 7/1/15.
//  Copyright (c) 2015 Dimitar Dimitrov. All rights reserved.
//

#import "DetailsViewController.h"

@implementation Word
@synthesize title;
@synthesize description;
@end


@implementation DetailsViewController {
	UILabel* titleView;
	UILabel* descriptionView;
}

-(id) initWithWord:(Word*) word {
	self = [super init];

	if (self) {
		[self setTitle:word.title];
		self.view.backgroundColor = [UIColor whiteColor];

		titleView = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, self.view.frame.size.width - 20, 100)];
		titleView.text = word.title;
		titleView.textAlignment = NSTextAlignmentCenter;
		titleView.textColor = [UIColor orangeColor];
		titleView.lineBreakMode = NSLineBreakByWordWrapping;
		titleView.numberOfLines = 0;
		titleView.font = [UIFont boldSystemFontOfSize:18];

		descriptionView = [[UILabel alloc] initWithFrame:CGRectMake(20, 150, self.view.frame.size.width - 50, self.view.frame.size.height - 200)];
		descriptionView.text = word.description;
		descriptionView.textColor = [UIColor darkGrayColor];
		descriptionView.lineBreakMode = NSLineBreakByWordWrapping;
		descriptionView.numberOfLines = 0;
		descriptionView.font = [UIFont italicSystemFontOfSize:15];

		[self.view addSubview:descriptionView];
		[self.view addSubview:titleView];
	}

	return self;
}

-(void) viewDidLoad {
	[super viewDidLoad];

	
}

@end
