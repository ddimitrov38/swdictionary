//
//  ViewController.m
//  SWDictionary
//
//  Created by Dimitar Dimitrov on 7/1/15.
//  Copyright (c) 2015 Dimitar Dimitrov. All rights reserved.
//

#import "ViewController.h"
#import "DetailsViewController.h"
#include <sqlite3.h>

@interface ViewController () {
	sqlite3 *_database;

	UITableView *tableView;
	NSMutableDictionary *dictionary;

	NSArray *sortedIndexKeys;

	UISearchBar *searchBar;

	UIView *overlayView;

	BOOL inSearchMode;
}

@end

static NSString *cellIdentifier = @"WordCell";


@implementation ViewController

-(id) init {
	self = [super init];

	if (self) {
		self.view.backgroundColor = [UIColor whiteColor];

		CGRect frame = self.view.bounds;
		frame.size.height -= self.navigationController.navigationBar.frame.size.height;

		inSearchMode = NO;


		tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
		tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
		[tableView setDataSource:self];
		[tableView setDelegate:self];
		[self.view addSubview:tableView];

		searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 44)];
		[searchBar setDelegate:self];
		UIView *tableHeaderView = [[UIView alloc] initWithFrame:searchBar.frame];
		[tableHeaderView addSubview:searchBar];
		[tableView setTableHeaderView:tableHeaderView];

		NSString *sqLiteDb = [[NSBundle mainBundle] pathForResource:@"severozapad" ofType:@"sqlite3"];
		if (sqlite3_open_v2(sqLiteDb.UTF8String, &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK)
			NSLog(@"Failed to open database!");

		// Overlay
		overlayView = [[UIView alloc] initWithFrame:frame];
		overlayView.backgroundColor = [UIColor blackColor];
		overlayView.alpha = 0.0f;
		[self.view addSubview:overlayView];

		dictionary = [NSMutableDictionary dictionary];
		[self loadData];
	}

	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];

	[self setTitle:@"Речник Северозапад"];
}

#pragma mark UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return dictionary.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSArray *words = [dictionary objectForKey:[sortedIndexKeys objectAtIndex:section]];

	return words.count;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSArray *words = [dictionary objectForKey:[sortedIndexKeys objectAtIndex:indexPath.section]];
	Word *word = [words objectAtIndex:indexPath.row];

	UITableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:cellIdentifier];

	if (cell == nil)
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];

//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

	cell.textLabel.text = word.title;
	cell.textLabel.textColor = [UIColor orangeColor];
	cell.textLabel.font = [UIFont boldSystemFontOfSize:18];

	cell.detailTextLabel.text = word.description;
	cell.detailTextLabel.textColor = [UIColor darkGrayColor];
	cell.detailTextLabel.font = [UIFont italicSystemFontOfSize:15.0f];

	return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return [sortedIndexKeys objectAtIndex:section];
}

#pragma mark UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 70;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
	return sortedIndexKeys;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSArray *words = [dictionary objectForKey:[sortedIndexKeys objectAtIndex:indexPath.section]];

	DetailsViewController *detailsVC = [[DetailsViewController alloc] initWithWord:[words objectAtIndex:indexPath.row]];
	[self.navigationController pushViewController:detailsVC animated:YES];
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	CGRect searchBarFrame = searchBar.frame;

	if (inSearchMode)
		searchBarFrame.origin.y = scrollView.contentOffset.y;
	else
		searchBarFrame.origin.y = MIN(0, scrollView.contentOffset.y);

	searchBar.frame = searchBarFrame;
}

#pragma mark UISearchBarDelegate
- (void) searchBarSearchButtonClicked:(UISearchBar*) theSearchBar {
	[searchBar resignFirstResponder];
	[searchBar setShowsCancelButton:NO animated:YES];

	NSLog(@"Search for %@", searchBar.text);
	[searchBar setText:@""];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) theSearchBar {
	[self searchBarTextDidEndEditing:theSearchBar];
	[searchBar setText:@""];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *) theSearchBar {
	[searchBar setShowsCancelButton:YES animated:YES];

	tableView.allowsSelection = NO;
	tableView.scrollEnabled = NO;

	[searchBar removeFromSuperview];
	[overlayView addSubview:searchBar];

	[UIView animateWithDuration:0.5f animations:^{
		overlayView.alpha = 0.8f;
	}];

	inSearchMode = YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *) theSearchBar {
	[searchBar resignFirstResponder];
	[searchBar setShowsCancelButton:NO animated:YES];

	tableView.allowsSelection = YES;
	tableView.scrollEnabled = YES;

	[searchBar removeFromSuperview];
	[tableView.tableHeaderView addSubview:searchBar];

	[UIView animateWithDuration:0.5f animations:^{
		overlayView.alpha = 0.0f;
	}];

	inSearchMode = NO;
}



-(void) loadData {
	NSString *query = @"select id, word, description, type_id, stamp, linked from diction order by word asc";

	sqlite3_stmt *stmt;
	int sqlite_res = sqlite3_prepare(_database, [query UTF8String], -1, &stmt, nil);
	if (sqlite_res == SQLITE_OK) {
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			Word* word = [[Word alloc] init];

			word.title = [[NSString alloc] initWithUTF8String: (char *) sqlite3_column_text(stmt, 1)];
			if (sqlite3_column_text(stmt, 2) != NULL)
				word.description = [[NSString alloc] initWithUTF8String: (char *) sqlite3_column_text(stmt, 2)];
			else
				word.description = @"Missing description";

			NSString* indexKey = [word.title substringToIndex:1].uppercaseString;
			if ([dictionary objectForKey:indexKey]) {
				NSMutableArray* array = [dictionary objectForKey:indexKey];
				[array addObject:word];
			} else {
				NSMutableArray* array = [NSMutableArray arrayWithObjects:word, nil];
				[dictionary setObject:array forKey:indexKey];
			}

		}
	} else {
		NSLog(@"ERROR: %s", sqlite3_errstr(sqlite_res));
	}
	sqlite3_finalize(stmt);

	sortedIndexKeys = [[dictionary allKeys] sortedArrayUsingSelector:@selector(compare:)];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

@end
