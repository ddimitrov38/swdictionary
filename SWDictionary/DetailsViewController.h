//
//  DetailsViewController.h
//  SWDictionary
//
//  Created by Dimitar Dimitrov on 7/1/15.
//  Copyright (c) 2015 Dimitar Dimitrov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Word : NSObject
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *description;
@end



@interface DetailsViewController : UIViewController

-(id) initWithWord:(Word*) word;
@end
