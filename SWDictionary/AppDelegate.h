//
//  AppDelegate.h
//  SWDictionary
//
//  Created by Dimitar Dimitrov on 7/1/15.
//  Copyright (c) 2015 Dimitar Dimitrov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

